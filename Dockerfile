#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=opensearch
ENV APP_GROUP=opensearch

ENV OPENSEARCH_DASHBOARDS_VERSION=2.16.0

# Create app user and group
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /usr/local/share/opensearch-dashboards -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS wget

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src
RUN wget -nv https://artifacts.opensearch.org/releases/bundle/opensearch-dashboards/${OPENSEARCH_DASHBOARDS_VERSION}/opensearch-dashboards-${OPENSEARCH_DASHBOARDS_VERSION}-linux-x64.tar.gz
RUN mkdir -p /usr/local/share/opensearch-dashboards
RUN tar -zxf opensearch-dashboards-${OPENSEARCH_DASHBOARDS_VERSION}-linux-x64.tar.gz -C /usr/local/share/opensearch-dashboards --strip-components=1


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV PATH=/usr/local/share/opensearch-dashboards/bin:$PATH

RUN chown -R ${APP_USER}:0 /usr/local/share/opensearch-dashboards

EXPOSE 5601/tcp
VOLUME ["/etc/opensearch-dashboards"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["opensearch-dashboards"]
