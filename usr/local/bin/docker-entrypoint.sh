#!/bin/bash

set -e

if [ "$1" = 'opensearch-dashboards' ]; then
    exec opensearch-dashboards --config /etc/opensearch-dashboards/opensearch-dashboards.yml
fi

exec "$@"